<?php

/*****************************************************************************
 *
 *	copyright(c) - aonetheme.com - Service Finder Team
 *	More Info: http://aonetheme.com/
 *	Coder: Service Finder Team
 *	Email: contact@aonetheme.com
 *
 ******************************************************************************/
?>
<?php
wp_enqueue_script('service_finder-js-upgrade');
$service_finder_options = get_option('service_finder_options');
$wpdb = service_finder_plugin_global_vars('wpdb');
$service_finder_Tables = service_finder_plugin_global_vars('service_finder_Tables');
$paymentsystem = service_finder_plugin_global_vars('paymentsystem');
$user_id = get_current_user_id();

wp_add_inline_script('service_finder-js-upgrade', '/*Declare global variable*/
var user_id = "' . $user_id . '";

', 'after');
// $row = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $service_finder_Tables->feature . ' WHERE `provider_id` = %d', $globalproviderid));
// $current_role = get_user_meta($globalproviderid, 'provider_role', true);
?>




<div class="panel panel-default">

    <div class="panel-heading sf-panel-heading">

        <h3 class="panel-tittle m-a0"><span class="fa fa-gear"></span> <?php echo (!empty($service_finder_options['label-upgrade'])) ? esc_html($service_finder_options['label-upgrade']) : esc_html__('Upgrade Account', 'service-finder'); ?> </h3>

    </div>

    <div class="panel-body sf-panel-body padding-30">

        <?php if ($service_finder_options['customer-upgrade'] && service_finder_getUserRole($user_id) == 'Customer') { ?>

            <?php
            $expire_limit = get_user_meta($user_id, 'expire_limit', true);
            ?>

            <!--Request to make feature-->
            <div id="feature-req-bx">
                <?php
                if (isset($expire_limit) && $expire_limit > 0) {
                    $provider_activation_time = get_user_meta($user_id, 'provider_activation_time', true);
                    $timeInSec = $provider_activation_time['time'];
                    $activationdate = date('Y-m-d', $timeInSec);
                    $date = new DateTime($activationdate);
                    $date->add(new DateInterval('P' . $expire_limit . 'D'));
                    $expiredate = $date->format('Y-m-d');
                    $expired = strtotime($expiredate) - strtotime(date('Y-m-d'));
                    if ($expired == 0) {
                        update_user_meta($user_id, 'expire_limit', $expired);
                    }
                ?>
                    <div class="sf-select-plan">
                        <div class="sf-plan-left-block">
                            <h1><?php echo esc_html__('Current Package', 'service-finder'); ?></h1>
                            <div class="sf-renew-btn"><a class="btn btn-renew renewpackage"  href="javascript:;" style="pointer-events: none; "><i class="fa"></i> <?php echo esc_html__('Active for a year', 'service-finder') ?></a></div>
                        </div>
                        <div class="sf-plan-right-block">
                            <div class='countdown' data-date="<?php echo esc_attr($expiredate) ?>"></div>
                        </div>
                    </div>
                <?php echo '<div class="alert-bx alert-info">' . esc_html__('Your account has already been upgraded for a period of one year.', 'service-finder') . '</div>';
                } else { ?>
                    <!--  -->
                <?php echo '<div class="alert-bx alert-info">' . esc_html__('Upgrading to premium gives you additional benefits,you will be able to see all search results. Upgrade at a minimal cost of $'. $service_finder_options['customer-upgrade-price'] .'per year', 'service-finder') . '</div>' ?>
                    <form class="feature-form" method="post">
                        <div class="col-lg-12">
                            <div class="form-group form-inline">
                                <label>
                                    <?php esc_html_e('Upgrade profile', 'service-finder'); ?>
                                </label>
                                <br>
                                <div class="checkbox sf-radio-checkbox">
                                    <input type="checkbox" id="make-feature" name="make-feature" value="yes">
                                    <label for="make-feature"></label>
                                </div>
                            </div>

                        </div>

                        <div id="feature-bx" class="default-hidden">

                            <div class="col-lg-12">

                                <div class="form-group">

                                    <label>

                                        <?php esc_html_e('Number of Months', 'service-finder'); ?>

                                    </label>

                                    <div class="input-group">

                                        <?php /*?><input type="text" readonly class="form-control sf-form-control" name="featuredays" id="featuredays" value="<?php echo (!empty($service_finder_options['feature-min-max-days'][1])) ? esc_attr($service_finder_options['feature-min-max-days'][1]) : '' ?>" ><?php */ ?>

                                        <input type="hidden" name="minvalue" id="minvalue" value="<?php echo (!empty($service_finder_options['feature-min-max-days'][1])) ? esc_attr($service_finder_options['feature-min-max-days'][1]) : '' ?>">

                                        <input type="hidden" name="maxvalue" id="maxvalue" value="<?php echo (!empty($service_finder_options['feature-min-max-days'][2])) ? esc_attr($service_finder_options['feature-min-max-days'][2]) : '' ?>">

                                        <select name="featuredays" class="sf-select-box form-control sf-form-control bs-select-hidden fe_featured_days_num" id="featuredays">
                                            <option value="365" data-p_amount="<?php echo $service_finder_options['customer-upgrade-price'] ?>" selected>for 12 Months </option>
                                            <!-- <option value="60" data-p_amount="2">Featured for 2 Months</option>
                                                <option value="90" data-p_amount="3">Featured for 3 Months</option> -->
                                        </select>


                                    </div>

                                </div>

                            </div>
                            <?php if ($paymentsystem == 'woocommerce') {
                                echo '<div class="col-lg-12">
				                        <div class="form-group form-inline">';
                                echo service_finder_add_wallet_option('feature_woopayment', 'feature');
                                echo service_finder_add_woo_commerce_option('feature_woopayment', 'feature');
                                echo '</div></div>';
                            ?>


                                <div class="col-md-12">

                                    <input type="hidden" name="user_id" value="<?php echo esc_attr($user_id); ?>">
                                    <input type="hidden" name="amount" id="fe_amount" value="2">

                                    <input type="submit" class="btn btn-primary btn-block make_feature_sbmt_btn" name="feature-request" value="<?php esc_html_e('Pay Now', 'service-finder'); ?>" />

                                </div>
                            <?php
                            } ?>

                        </div>

                    </form>
                    <script>
                        $ = jQuery;
                        $(document).ready(function(e) {
                            var p_amount_num = $('.fe_featured_days_num').find(':selected').attr('data-p_amount');
                            $('#fe_amount').val(p_amount_num);
                            $('.fe_featured_days_num').change(function() {
                                var p_amount_num = $(this).find(':selected').attr('data-p_amount');
                                $('#fe_amount').val(p_amount_num);

                            });
                        });
                    </script>
                    <!--  -->

                <?php //echo '<div class="alert-bx alert-info">' . esc_html__('You have already made a request for feature. Please wait for approval.', 'service-finder') . '</div>';
                }

                ?>

            </div>

            <?php // }
            ?>

        <?php } ?>

    </div>


</div>