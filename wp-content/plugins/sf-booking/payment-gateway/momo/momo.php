<?php

/**
 * MTN MOBILE MONEY API FUNCTIONS
 *--------------------------------------------------------------------------------------------
 */



class MomoGateway
{

    private $error_message;
    private $access_token;
    private $api_user;
    private $api_key;
    private $subscription_key;
    private $environment; //= "sandbox"; // mtncameroon, mtnug
    private $host; //= ;

    public function __construct()
    {
        $service_finder_options = get_option('service_finder_options');
        $this->subscription_key = $service_finder_options['momo-subscriptionkey'];
        $this->environment = $service_finder_options['momo-type']; //= "sandbox"; // mtncameroon, mtnug
        $this->host = 'https://sandbox.momodeveloper.mtn.com';
        $this->api_user = $service_finder_options['momo-apiuser'];
        $this->api_key = $service_finder_options['momo-apikey'];
    }

    // Start session if it hasn't been started yet
    public function start_session()
    {
        if (!session_id()) {
            session_start();
        }
    }

    public function gen_uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }


    /**
     * payment request
     * @param: $amount
     * @param:$currency
     * @param: $userId
     * @param:$userIdType
     * @param: $payeeNote
     * @param:$payerMessage
     * @param: $callbackUrl
     * @param: $externalId
     * @return: string when successful and array when it fails
     */
    public function request_to_pay(
        $amount,
        $currency,
        $userIdType,
        $userId,
        $payerMessage,
        $payeeNote,
        $callbackUrl,
        $externalId
    ) {
        // $amount = 2;
        $feedback = array();
        // $amount = ceil((int)$amount) . '';
        $body = json_encode([
            'amount' => $amount,
            'currency' => $currency,
            'externalId' => $externalId,
            'payer' => [
                'partyIdType' => $userIdType,
                'partyId' => $userId
            ],
            'payerMessage' => $payerMessage,
            'payeeNote' => $payeeNote,
        ]);
        // wc_add_notice(__('Request body: ', 'woo-mobipay') . json_encode($body), 'error');

        $access_token = $this->access_token;
        // Set reference ID and place in session
        $reference_id = $_SESSION['reference_id'];

        $headers = [
            'Authorization' => 'Bearer ' . $access_token,
            //'X-Callback-Url' => $callbackUrl,
            'X-Reference-Id' => $reference_id,
            'X-Target-Environment' => $this->environment,
            'Content-Type' => 'application/json',
            'Ocp-Apim-Subscription-Key' => $this->subscription_key
        ];

        $args = [
            'body' => $body,
            'headers' => $headers
        ];
        $response = wp_remote_post($this->host . '/collection/v1_0/requesttopay', $args);
        if (is_wp_error($response)) {
            $feedback['has_succeeded'] = false;
            $feedback['message'] = $response->get_error_message();
            return $feedback;
        } else {

            $status_code = wp_remote_retrieve_response_code($response);
            return $status_code;
        }

        // $parsedBody = json_decode($body);

        // return $parsedBody;
    } // end request_to_pay

    /**
     * verify request to pay
     * @return: $response
     */
    public function verify_request_to_pay()
    {

        $reference_id = $_SESSION['reference_id'];

        $headers = [
            'Authorization' => 'Bearer ' . $this->access_token,
            //'X-Reference-Id' => $reference_id,
            'X-Target-Environment' => $this->environment,
            'Content-Type' => 'application/json',
            'Ocp-Apim-Subscription-Key' => $this->subscription_key
        ];

        $args = [
            // 'body' => $body,
            'headers' => $headers
        ];

        $url = $this->host . '/collection/v1_0/requesttopay/' . $reference_id;

        $response = wp_remote_get($url, $args);

        // wc_add_notice(__('Transfer URL: ', 'woo-mobipay') . $url, 'error');
        // wc_add_notice(__('Response body: ', 'woo-mobipay') . wp_remote_retrieve_body($response), 'error');

        return $response;
    }
    /**
     * loop through request t pay
     * @return: array
     */

    public function request_to_pay_loop()
    {

        $tries = 0;
        $TRY_INCREMENTS = 30;
        $MAX_TIME = 120;
        $MAX_TRIES = (int) ($MAX_TIME / $TRY_INCREMENTS);
        $message = '';
        $has_succeeded = false;

        // wc_add_notice(__('$MAX_TRIES', 'woo-mobipay') . $MAX_TRIES, 'error');

        // while ($tries * $TRY_INCREMENTS < $MAX_TIME) {
        while ($tries < $MAX_TRIES) { // $MAX_TRIES

            $tries++;

            $response = $this->verify_request_to_pay();
            $status_code = wp_remote_retrieve_response_code($response);


            // wc_add_notice(__('Unknown error $status_code.', 'woo-mobipay') . $status_code, 'error');

            if ((int)$status_code >= 200 && (int)$status_code < 300) {

                $body = wp_remote_retrieve_body($response);
                $parsedBody = json_decode($body);

                $phone = $_POST['momo_number'];


                // Manually set response codes in sandbox as test numbers are not all giving the desired response
                switch ($phone) {
                    case '46733123450':
                        $status = 'FAILED';
                        break;
                    case '46733123451':
                        $status = 'REJECTED';
                        break;
                    case '46733123452':
                        $status = 'TIMEOUT';
                        break;
                    case '46733123453':
                        if ($tries > 1) {
                            $status = 'SUCCESSFUL';
                        } else {
                            $status = 'PENDING';
                        }
                        break;
                    case '46733123454':
                        $status = 'PENDING';
                        break;
                    default:
                        $status = $parsedBody->status;
                        break;
                }

                if ($status == 'SUCCESSFUL') {
                    $message = '';
                    $has_succeeded = true;

                    break;
                } elseif ($status == 'PENDING') {
                    $message = __('Transaction timed out.', 'zinger-momo-pay');
                    sleep($TRY_INCREMENTS);
                    continue;
                } elseif ($status == 'TIMEOUT') {
                    $message = __('Transaction timed out.', 'zinger-momo-pay');
                    $has_succeeded = false;

                    break;
                } elseif ($status == 'REJECTED') {
                    $message = __('User rejected this transaction.', 'zinger-momo-pay');
                    $has_succeeded = false;

                    break;
                } elseif ($status == 'FAILED') {
                    $message = __('Transaction failed.', 'zinger-momo-pay');
                    $has_succeeded = false;

                    break;
                } else {
                    $message = __('Unknown error.', 'zinger-momo-pay');
                    $has_succeeded = false;
                    break;
                }
            } else {
                $message = __('Transaction failed.', 'zinger-momo-pay');
                $has_succeeded = false;
            }
        }

        return array(
            'has_succeeded' => $has_succeeded,
            'message' => $message
        );
    }
    /**
     * Get auth token
     *
     * @return string or array               Retrieved access token.
     */
    public function get_token()
    {

        $username = $this->api_user;
        $password = $this->api_key;
        $auth = $username . ':' . $password;
        $basic_token = 'Basic ' . base64_encode($auth);

        $args = [
            'body' => [],
            'headers' => [
                'Authorization' => $basic_token,
                'Ocp-Apim-Subscription-Key' => $this->subscription_key,
            ]
        ];
        $response = wp_remote_post($this->host . '/collection/token/', $args);
        if (is_wp_error($response)) {
            $feedback['has_succeeded'] = false;
            $feedback['message'] = $response->get_error_message();
            return $feedback;
        } else {
            $body = wp_remote_retrieve_body($response);
            if ($body != null && !empty($body)) {
                $return = json_decode($body);

                if (isset($return->access_token) && $return->access_token != null && !empty($return->access_token)) {
                    return $return->access_token;
                }
            }
        }
    } // end get_token
    /**
     * process payment
     * @param: $amount
     * @param: $tel
     * @param: $currency
     * @return: $feedback array()
     *
     */

    public function process_payment($amount, $tel, $currency)
    {
        $feedback = array();



        $x_reference_id = $this->gen_uuid();

        $this->start_session();
        $_SESSION['reference_id']     = $x_reference_id;


        if ($this->environment != 'sandbox') {
            $this->host  = 'https://ericssonbasicapi1.azure-api.net';
            $tel = '237' . $tel; // TODO handle other countries
        }


        // Retrieve access token
        $access_token = $this->get_token();

        if (!empty($access_token)) {

            // Place access token in session
            $this->access_token = $access_token;

            // Set external ID
            $external_id = $tel . '-' . time();

            $amount = anevCurrencyConverter($amount, service_finder_currencycode(), 'XAF');

            $currency = ($this->environment == 'sandbox') ? 'EUR' : 'XAF';
            // Make MoMo payment
            $amount =  ceil($amount);
 
            $status_code = $this->request_to_pay(
                $amount,
                $currency,
                'MSISDN',
                $tel,
                'Payer message',
                'Payee note',
                null,
                $external_id
            );

            if ((int)$status_code === 202) {

                $loop_response = $this->request_to_pay_loop();


                if ($loop_response['has_succeeded'] === true) {

                    $feedback['has_succeeded'] = true;
                    $feedback['message'] = __('payment successful', 'zinger-momo-pay');

                    return $feedback;
                } else {
                    $feedback['has_succeeded'] =  false;
                    $feedback['message'] = $loop_response['message'];

                    return $feedback;
                }
            } else {
                $feedback['has_succeeded'] = false;
                $feedback['message'] = __('Sorry, we were unable to initiate transaction. Please try again.', 'zinger-momo-pay');
                return $feedback;
            }
        }

        // echo json_encode( array(
        //     'result' => 'danger',
        // ));
    }

    /*Make Momo Payment for Feature*/

    public function service_finder_makePayment($args = '', $payment_mode = '')
    {

        global $wpdb, $service_finder_Tables;

        $date = date('Y-m-d H:i:s');

        $data = array(

            'paymenttype' => $payment_mode,
            'status' => 'Paid',
            'feature_status' => 'active',
            'date' => $date,

        );

        $where = array(

            'id' => esc_attr($args['feature_id'])

        );

        $res = $wpdb->update($service_finder_Tables->feature, wp_unslash($data), $where);



        if (!$res) {

            $adminemail = get_option('admin_email');

            $allowedhtml = array(

                'a' => array(

                    'href' => array(),

                    'title' => array()

                ),

            );

            $error = array(

                'status' => 'error',

                'err_message' => sprintf(wp_kses(esc_html__('Couldn&#8217;t make payment for feature... please contact the <a href="mailto:%s">Administrator</a> !', 'service-finder'), $allowedhtml), $adminemail)

            );

            $service_finder_Errors = json_encode($error);

            return $service_finder_Errors;
        } else {

            $msg = (!empty($service_finder_options['feature-payment'])) ? $service_finder_options['feature-payment'] : esc_html__('Payment made successfully to be featured', 'service-finder');


            $success = array(

                'status' => 'success',

                'suc_message' => $msg,

            );

            $service_finder_Success = json_encode($success);

            return $service_finder_Success;
        }
    }
}
