<?php
// Orange Money return page
require_once("../../../../../wp-load.php");

$_SESSION['response'] = __('Sorry we could not complete your request because we could not process your payment. If you feel this is an error, please send us a complaint at webmaster@huslamarket.com', 'maajik');
wp_redirect($_SESSION['redirect_url']);
