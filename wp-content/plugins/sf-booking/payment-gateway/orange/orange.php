<?php

/**
 * ORANGE MONEY API FUNCTIONS
 *--------------------------------------------------------------------------------------------
 */

class OrangeMoneyGateway
{

    private $access_token;
    private $host; //= 'https://api.orange.com';
    private $pay_token;
    private $order_id_pay;
    private $merchantKey;
    private $clientID; // = get_option('zin_client_id');
    private $clientSecret; // = get_option('zin_client_secret');
    private $appId;

    public function __construct()
    {
        $service_finder_options = get_option('service_finder_options');
        $this->merchantKey = $service_finder_options['orange-merchantkey'];
        $this->appID = $service_finder_options['orange-appid'];
        $this->host = 'https://api.orange.com';
        $this->clientID = $service_finder_options['orange-clientid']; // = get_option('zin_client_id');
        $this->clientSecret = $service_finder_options['orange-clientsecret']; // = get_option('zin_client_secret');

    }

    /**
     * Get auth token
     *
     * @return string               Retrieved access token.
     */

    public function get_token()
    {
        $clientID = $this->clientID;
        $clientSecret = $this->clientSecret; //get_option('zin_client_secret');



        $auth = $clientID . ':' . $clientSecret;
        $basic_token = 'Basic ' . base64_encode($auth);

        $args = [
            'body' => array(
                'grant_type' => 'client_credentials',
            ),
            'headers' => [
                'Authorization' => $basic_token,
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ];

        $response = wp_remote_post($this->host . '/oauth/v2/token', $args);

        $body = wp_remote_retrieve_body($response);


        if ($body != null && !empty($body)) {
            $return = json_decode($body);

            if (isset($return->access_token) && $return->access_token != null && !empty($return->access_token)) {

                return $return->access_token;
                // $error = array(
                //     'status' => 'error',
                //     'err_message' => sprintf(esc_html__('%s', 'service-finder'), $return->access_token),
                //     'response' =>  $return->access_token,
                //     'token' => $return->access_token,
                // );
                // echo json_encode($error);
            }
        } else {
            return '';
        }

        //return '';
    } // end get_token


    public function request_to_pay($amount, $redirect_url, $merchantKey, $order_id, $access_token)
    {
        $amount = $amount . '';

        // $order = new WC_Order($order_id);

        $body = json_encode([
            'merchant_key' => $merchantKey,
            'currency' => 'XAF',
            'order_id' => $order_id,
            'amount' => '' . $amount,
            'return_url' => get_site_url() . '/wp-content/plugins/sf-booking/payment-gateway/orange/redirect_page.php',
            'cancel_url' => get_site_url() . '/wp-content/plugins/sf-booking/payment-gateway/orange/cancel_page.php',
            'notif_url' => get_site_url(),
            'lang' => substr(get_locale(), 0, 2),
            'reference' => esc_html__('Huslajobs', 'service-finder'),
        ]);


        $headers = [
            'Authorization' => 'Bearer ' . $access_token,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];

        $args = [
            'body' => $body,
            'headers' => $headers
        ];

        $response = wp_remote_post($this->host . '/orange-money-webpay/cm/v1/webpayment', $args);

        $body = wp_remote_retrieve_body($response);
        $dresponse = json_decode($body);

        $_SESSION['pay_token'] = $dresponse->pay_token;
        $_SESSION['om_order_id'] = $order_id;
        $_SESSION['om_amount'] = $amount;
        $_SESSION['om_access_token'] = $access_token;
        $_SESSION['om_payment_url'] = $dresponse->payment_url;
        $_SESSION['redirect_url'] = $redirect_url;
        $_SESSION['feature_id'] = $_POST['feature_id'];

        return $dresponse;
    } // end request_to_pay


    // Processes payments
    public function process_payment($amount, $redirect_url)
    {
        $this->start_session();

        $feedback = array();
       $amount = anevCurrencyConverter($amount, service_finder_currencycode(), 'XAF');
       $amount = ceil($amount);

        // $_SESSION['host'] = 'https://api.orange.com';
        $merchantKey = $this->merchantKey; // get_option('zin_merchant_key');
        $order_id = $merchantKey . '-' . time();

        // Retrieve access token
        $access_token = $this->get_token();




        if (!empty($access_token)) {


            $payment_request = $this->request_to_pay($amount, $redirect_url, $merchantKey, $order_id, $access_token);

            if ($payment_request->status == 201) {

                $feedback['has_succeeded'] = true;
                $feedback['message'] = 'payment successful';
                $feedback['om_payment_url'] = $_SESSION['om_payment_url'];
                return $feedback;
            } else {
                $feedback['has_succeeded'] = false;
                $feedback['message'] = __('Sorry, we were unable to initiate transaction. Please try again.', 'zinger-momo-pay');
                return $feedback;
            }
        } else {
            $feedback['has_succeeded'] = false;
            $feedback['message'] = __('You are unauthorized to access the Orange Money Gateway.', 'zinger-momo-pay');
            return $feedback;
        }
    }

    /**
     * Starts session if session has not started
     *
     * @return void
     */
    public function start_session()
    {
        if (!session_id()) {
            session_start();
        }
    }
}
