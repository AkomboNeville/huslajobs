<?php
// Orange Money return page

require_once("../../../../../wp-load.php");

$b = [
    "order_id" => $_SESSION['om_order_id'],
    "amount" => $_SESSION['om_amount'],
    "pay_token" => $_SESSION['pay_token']
];

$b = json_encode($b);
$args = [
    'headers' => [
        'Authorization' => 'Bearer ' . $_SESSION['om_access_token'],
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    ],
    'body' => $b
];

$response = wp_remote_post('https://api.orange.com/orange-money-webpay/cm/v1/transactionstatus', $args);

$status_code = wp_remote_retrieve_response_code($response);
$adminemail = get_option('admin_email');

$allowedhtml = array(

    'a' => array(

        'href' => array(),

        'title' => array()

    ),

);

if ($status_code === 201) {

    $body = wp_remote_retrieve_body($response);
    $parsedBody = json_decode($body);
    $status = $parsedBody->status;

    if ($status === "SUCCESS") {

        /*Make Momo Payment for Feature*/

        global $wpdb, $service_finder_Tables;

        $date = date('Y-m-d H:i:s');

        $data = array(

            'paymenttype' => 'orange',
            'status' => 'Paid',
            'feature_status' => 'active',
            'date' => $date,

        );

        $where = array(
            'id' => esc_attr($_SESSION['feature_id'])
        );


        $res = $wpdb->update($service_finder_Tables->feature, wp_unslash($data), $where);

        if (!$res) {

            $resp = array(
                'status' => false,
                'message' => sprintf(wp_kses(esc_html__('Couldn&#8217;t make payment for feature... please contact the Administrator at %s!', 'service-finder'), $allowedhtml), $adminemail)
            );

            $_SESSION['om_response'] = $resp;
        } else {
            $feature_id = (!empty($_SESSION['feature_id'])) ? esc_attr($_SESSION['feature_id']) : '';
            $row = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $service_finder_Tables->feature . ' WHERE `id` = %d', $feature_id));

            $limit = floatval($row->days);
            $displaymsg = (!empty($service_finder_options['featured-account'])) ? $service_finder_options['featured-account'] : esc_html__('Now you are a featured member. You have %REMAININGDAYS% days remaining to expire your feature account.', 'service-finder');
            $displaymsg = str_replace('%REMAININGDAYS%', $limit, $displaymsg);

            $resp = array(
                'status' => true,
                'message' => (!empty($service_finder_options['feature-payment'])) ? $service_finder_options['feature-payment'] : esc_html__('Payment made successfully to be featured', 'service-finder'),
                'display_message' => $displaymsg
            );
            $_SESSION['om_response']  = $resp; //(!empty($service_finder_options['feature-payment'])) ? $service_finder_options['feature-payment'] : esc_html__('Payment made successfully to be featured', 'service-finder');
        }
        //  Issue success message
    } else {

        $res = array(
            'status' => false,
            'message' => sprintf(wp_kses(esc_html__('Couldn&#8217;t make payment for feature... please contact the Administrator at %s!', 'service-finder'), $allowedhtml), $adminemail)
        );

        $_SESSION['om_response'] = $res;
    }
} else {

    $res = array(
        'status' => false,
        'message' => sprintf(wp_kses(esc_html__('Couldn&#8217;t make payment for feature... please contact the Administrator at %s !', 'service-finder'), $allowedhtml), $adminemail)
    );

    $_SESSION['om_response'] = $res;
}



// var_dump($_SESSION['om_response']);
// die();
wp_redirect($_SESSION['redirect_url']);
