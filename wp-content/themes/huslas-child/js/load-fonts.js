$(document).ready(function () {
    loadCSS("/wp-content/themes/huslas-child/css/font-awesome.css");
    loadCSS("/wp-content/themes/huslas-child/css/swiper.min.css");
    loadCSS("/wp-content/themes/huslas-child/css/jquery.fancybox.min.css");
    loadCSS("/wp-content/themes/huslas-child/css/intlTelInput.min.css");
    loadCSS("/wp-content/themes/huslas-child/css/fonts.css");

});

function loadCSS(href, before, media) {
    "use strict";
    var ss = window.document.createElement("link");
    var ref = before || window.document.getElementsByTagName("script")[0];
    ss.rel = "stylesheet";
    ss.href = href;
    ss.media = "only x";
    ref.parentNode.insertBefore(ss, ref);
    setTimeout(function () {
        ss.media = media || "all";
    });
    return ss;
}


