<?php
/**
 Template name: Sign Up main template
 */
global $post;
$post_slug=$post->post_name;
get_header(); ?>

<div class="sng_signup_main afclr">
<div class="wrapper">
<div class="sng_signup_title afclr"><h2>Sign up and Become a Husla</h2></div>
<div class="sng_signup_main_inner afclr">
<div class="sng_sign_logo_middle afclr"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/signup_logo_middle.png"></div>
<div class="sn_signup_left sn_signup_ser_provider">
<div class="sn_signup_le_inner afclr">
<h3>Service Provider</h3>
<?php the_field('service_provider_content'); ?>
<a href="<?php echo get_permalink(2216);?>" class="sng_btn sng_btn_white">Sign Up</a>
</div>
</div>
<div class="sn_signup_left sn_signup_customer">
<div class="sn_signup_le_inner afclr">
<h3>Customer</h3>
<?php the_field('customer_content'); ?>
<a href="<?php echo get_permalink(2218);?>" class="sng_btn sng_btn_green">Sign Up</a>

</div>
</div>

</div>
</div>
</div>

<?php get_footer(); ?>