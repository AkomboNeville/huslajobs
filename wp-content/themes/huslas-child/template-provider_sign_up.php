<?php
/**
 Template name: Provider Sign Up template
 */

get_header(); ?>
<div class="lgn_login_section lgn_provider_s_section afclr">
<div class="wrapper">

<div class="lgn_provider_se_inner afclr">
<?php echo do_shortcode('[service_finder_signup role="provider"]'); ?>

</div>
</div>
</div>


<?php get_footer(); ?>