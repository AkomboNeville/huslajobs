<?php
/**
 Template name: Service finder Login template
 */

get_header(); ?>
<div class="lgn_login_section afclr">
<div class="wrapper">

<div class="lgn_login_se_inner afclr">
<div class="lgn_login_title afclr"><h2>Login</h2></div>
<?php echo do_shortcode('[service_finder_login]'); ?>

</div>
</div>
</div>

<?php get_footer(); ?>