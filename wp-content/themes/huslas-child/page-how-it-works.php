<?php get_header();

 /* Template Name: 	How it works  */ 

?>

<style>.page-content{padding-bottom:0;}</style>
<h1 style="display:none;">How It Works</h1>
<div class="page-content">

<div class="candidate_section">

<div class="wrapper">

<div class="blue_bg_heading ">

<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/people-img.png" alt="For Candidates icon">For Customer</div>

<div class="process_type_one rightside_arrow afclr">

<div class="process_img_right imgsection_candidate">

<?php 
$customer_step_one_image = get_field('customer_step_one_image');
if( !empty( $customer_step_one_image ) ): ?>
    <img src="<?php echo esc_url($customer_step_one_image['url']); ?>" alt="<?php echo esc_attr($customer_step_one_image['alt']); ?>" />
<?php endif; ?>

</div>

<div class="process_left_text textsection_candidate">

<div class="cprocess_no"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/candidate_no_1.png" alt="number one icon"></div>

<div class="cprocess_text"><h2><?php the_field('customer_step_one_title');?></h2>

<p><?php the_field('customer_step_one_content');?></p>

</div>

</div>

<span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blue-down-arrow.png" alt="blue down arrow icon"></span>

</div>

<div class="process_type_one leftside_arrow afclr">

<div class="process_img_left imgsection_candidate">

<?php 
$customer_step_two_image = get_field('customer_step_two_image');
if( !empty( $customer_step_two_image ) ): ?>
    <img src="<?php echo esc_url($customer_step_two_image['url']); ?>" alt="<?php echo esc_attr($customer_step_two_image['alt']); ?>" />
<?php endif; ?>


</div>

<div class="process_right_text textsection_candidate">

<div class="cprocess_no"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/candidate_no_2.png" alt="number two icon"></div>

<div class="cprocess_text"><h2><?php the_field('customer_step_two_title');?></h2>

<p><?php the_field('customer_step_two_content');?></p>

</div>

</div>



<span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/blue-down-arrow.png" alt="blue down arrow icon2"></span>

</div>

<div class="process_type_one rightside_arrow afclr">

<div class="process_img_right imgsection_candidate">
<?php 
$customer_step_third_image = get_field('customer_step_third_image');
if( !empty( $customer_step_third_image ) ): ?>
    <img src="<?php echo esc_url($customer_step_third_image['url']); ?>" alt="<?php echo esc_attr($customer_step_third_image['alt']); ?>" />
<?php endif; ?>

</div>

<div class="process_left_text textsection_candidate">

<div class="cprocess_no"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/candidate_no_3.png" alt="number three icon"></div>

<div class="cprocess_text"><h2><?php the_field('customer_step_third_title');?></h2>

<p><?php the_field('customer_step_third_content');?></p>

</div>

</div>

</div>

</div>

</div>

<div class="employer_section">

<div class="wrapper">

<div class="blue_bg_heading blue_bg_heading_color_r">

<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/people-img.png" alt="For Employer icon">For Service Provider

</div>

<div class="process_type_one rightside_arrow afclr">

<div class="process_img_right imgsection_candidate">

<?php 
$provider_step_one_image = get_field('provider_step_one_image');
if( !empty( $provider_step_one_image ) ): ?>
    <img src="<?php echo esc_url($provider_step_one_image['url']); ?>" alt="<?php echo esc_attr($provider_step_one_image['alt']); ?>" />
<?php endif; ?>

</div>

<div class="process_left_text textsection_candidate">

<div class="cprocess_no"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/employer_process_1.png" alt="employer process 1 icon"></div>

<div class="cprocess_text"><h2><?php the_field('provider_step_one_title');?></h2>

<p><?php the_field('provider_step_one_content');?></p>

</div>

</div>

<span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/emplyer-down-arrow.png" alt="employer down arrow icon"></span>

</div>

<div class="process_type_one leftside_arrow afclr">

<div class="process_img_left imgsection_candidate">

<?php 
$provider_step_two_image = get_field('provider_step_two_image');
if( !empty( $provider_step_two_image ) ): ?>
    <img src="<?php echo esc_url($provider_step_two_image['url']); ?>" alt="<?php echo esc_attr($provider_step_two_image['alt']); ?>" />
<?php endif; ?>

</div>

<div class="process_right_text textsection_candidate">

<div class="cprocess_no"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/employer_process_2.png" alt="employer process 2 icon"></div>

<div class="cprocess_text"><h2><?php the_field('provider_step_two_title');?></h2>

<p><?php the_field('provider_step_two_content');?></p>

</div>

</div>



<span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/emplyer-down-arrow.png" alt="employer down arrow 2 icon"></span>

</div>

<div class="process_type_one rightside_arrow afclr">

<div class="process_img_right imgsection_candidate">

<?php 
$provider_step_third_image = get_field('provider_step_third_image');
if( !empty( $provider_step_third_image ) ): ?>
    <img src="<?php echo esc_url($provider_step_third_image['url']); ?>" alt="<?php echo esc_attr($provider_step_third_image['alt']); ?>" />
<?php endif; ?>

</div>

<div class="process_left_text textsection_candidate">

<div class="cprocess_no"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/employer_process_3.png" alt="employer process 3 icon"></div>

<div class="cprocess_text"><h2><?php the_field('provider_step_third_title');?></h2>

<p><?php the_field('provider_step_third_content');?></p>

</div>

</div>

</div>

</div>

</div>

</div>



<script>

$=jQuery;

$(window).load(function(e) {

$(".process_type_one").each(function(index, element) {

var imgheight = $(this).children(".imgsection_candidate").height();

var textheight = $(this).children(".textsection_candidate").height(); 

if(imgheight > textheight){

var heightdiff = (imgheight-textheight)/2;

$(this).children(".textsection_candidate").css("padding-top",heightdiff);

} else {

var heightdiff = (textheight-imgheight)/2;

$(this).children(".imgsection_candidate").css("padding-top",heightdiff);	

}

});   

});

$(window).resize(function(e) {

$(".process_type_one").each(function(index, element) {

var imgheight = $(this).children(".imgsection_candidate").height();

var textheight = $(this).children(".textsection_candidate").height(); 

if(imgheight > textheight){

var heightdiff = (imgheight-textheight)/2;

$(this).children(".textsection_candidate").css("padding-top",heightdiff);

} else {

var heightdiff = (textheight-imgheight)/2;

$(this).children(".imgsection_candidate").css("padding-top",heightdiff);	

}

});   

});







</script>

<?php 

get_footer();