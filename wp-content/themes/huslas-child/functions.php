<?php
// Import featured product related functionality
require_once dirname(__FILE__) . '/inc/customer-upgrade-actions.php';

//Child Theme Functions File
add_action('wp_enqueue_scripts', 'enqueue_wp_child_theme');
function enqueue_wp_child_theme()
{
	if ((esc_attr(get_option('childthemewpdotcom_setting_x')) != "Yes")) {
		//This is your parent stylesheet you can choose to include or exclude this by going to your Child Theme Settings under the "Settings" in your WP Dashboard
		wp_enqueue_style('parent-css', get_template_directory_uri() . '/style.css');
	}

	//This is your child theme stylesheet = style.css
	wp_enqueue_style('child-css', get_stylesheet_uri());

	//This is your child theme stylesheet = style.css
	wp_deregister_script('service_finder-js-custom'); //, get_template_directory_uri() . '/inc/js/custom.js', array('jquery'), null, true);

	wp_enqueue_script('service_finder-js-custom', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), 'null', true);
	wp_enqueue_script('load-fonts', get_stylesheet_directory_uri() . '/js/load-fonts.js', array('jquery'), 'null', true);
	wp_enqueue_style('fonts-css', get_stylesheet_directory_uri() . '/css/style.css');
	// wp_enqueue_style('custom-theme-css', get_stylesheet_directory_uri() . '/css/theme.css');
	//This is your child theme js file = js/script.js
	// wp_deregister_script('wc-checkout');
	// wp_enqueue_script('child-js', get_stylesheet_directory_uri() . '/js/site_functions.js', array( 'jquery' ), '1.0', true );
}



function hj_deregister_scripts()
{
	// wp_deregister_script('map');

	wp_dequeue_script('map');
	wp_deregister_script('map');

	wp_dequeue_script('service_finder-load-home-map');
	wp_deregister_script('service_finder-load-home-map');

	wp_dequeue_script('service_finder-js-gmapfunctions');
	wp_deregister_script('service_finder-js-gmapfunctions');

	wp_dequeue_script('marker-clusterer');
	wp_deregister_script('marker-clusterer');

	wp_dequeue_script('markerinfo');
	wp_deregister_script('markerinfo');

	wp_dequeue_script('modernizr');
	wp_deregister_script('modernizr');

	wp_dequeue_script('marker-spider');
	wp_deregister_script('marker-spider');

	wp_dequeue_script('richmarker-compiled');
	wp_deregister_script('richmarker-compiled');
}
add_action('wp_print_scripts', 'hj_deregister_scripts');

// ChildThemWP.com Settings
function childthemewpdotcom_register_settings()
{
	register_setting('childthemewpdotcom_theme_options_group', 'childthemewpdotcom_setting_x', 'ctwp_callback');
}
add_action('admin_init', 'childthemewpdotcom_register_settings');

function childthemewpdotcom_register_options_page()
{
	add_options_page('Child Theme Settings', 'My Child Theme', 'manage_options', 'childthemewpdotcom', 'childthemewpdotcom_theme_options_page');
}
add_action('admin_menu', 'childthemewpdotcom_register_options_page');

if (function_exists('acf_add_options_page')) {


	acf_add_options_page(array(

		'page_title' => 'Site Options',

		'menu_title' => 'Site Options',

		'menu_slug' => 'options',
		'capability' => 'edit_posts',

		'redirect' => false

	));
}

register_nav_menus(array(
	'primary' => esc_html__('Primary Menu', 'service-finder'),
	'footer_menu' => esc_html__('Footer Menu', 'service-finder'),
	'footer_category_menu' => esc_html__('Footer Category Menu', 'service-finder'),
));

add_image_size('home_post', 364, 247, true);

add_action('template_redirect', 'wpse8170_activate_user');
function wpse8170_activate_user()
{
	if (is_page() && get_the_ID() == 2617) {
		$user_id = filter_input(INPUT_GET, 'user', FILTER_VALIDATE_INT, array('options' => array('min_range' => 1)));
		if ($user_id) {
			// get user meta activation hash field
			$code = get_user_meta($user_id, 'has_to_be_activated', true);
			if ($code == filter_input(INPUT_GET, 'key')) {
				delete_user_meta($user_id, 'has_to_be_activated');
				$user_roles = get_user_meta($user_id, 'hus_capabilities', true);
				$u = new WP_User($user_id);
				if (isset($user_roles['pending_customer'])) {

					$u->remove_role('pending_customer');
					$u->add_role('Customer');
				}
				if (isset($user_roles['pending_provider'])) {

					$u->remove_role('pending_provider');
					$u->add_role('Provider');
				}
			}
		}
	}
}
add_role(
	'pending_customer',
	__('Pending customer', 'customer registered but not verified'),
	array()
);
add_role(
	'pending_provider',
	__('Pending provider', 'provider registered but not verified'),
	array()
);

function wpse27856_set_content_type()
{
	return "text/html";
}
add_filter('wp_mail_content_type', 'wpse27856_set_content_type');


/**
 * function redirects users to the my-account
 * page if they don't have a profile pic
 *
 * @return void
 */
function huslas_redirect_users()
{
	global $pagenow;
	$service_finder_options = get_option('service_finder_options');
	$userInfo = service_finder_getCurrentUserInfo();

	$url_str = home_url($_SERVER['REQUEST_URI']);
	$tabname = (isset($_GET['tabname'])) ? esc_html($_GET['tabname']) : '';
	$manageaccountby = (isset($_GET['manageaccountby'])) ? esc_attr($_GET['manageaccountby']) : '';
	$manageproviderid = (isset($_GET['manageproviderid'])) ? esc_attr($_GET['manageproviderid']) : '';
	$current_user = service_finder_plugin_global_vars('current_user');


	if (!current_user_can('administrator')) {

		if ($current_user->ID) {
			if (service_finder_getUserRole($current_user->ID) == 'Provider' || service_finder_check_account_authorization($manageaccountby, $manageproviderid)) {

				if (service_finder_getUserRole($current_user->ID) == 'Provider') {

					$userInfo = service_finder_getCurrentUserInfo();
				} else {

					$userInfo = service_finder_getUserInfo($manageproviderid);
				}
			}



			$user_profile_image = service_finder_get_user_profile_image($userInfo['avatar_id']);
			if (!$user_profile_image) {
				$service_finder_options['myaccount-notification'] = esc_html__('Please upload  your profile picture ', 'service-finder');
				$_GET['tabname'] = 'my-profile';
				$regex = "/my-account/i";

				if (!preg_match($regex, $url_str, $match) && !(isset($_GET['action']) && $_GET['action'] == 'logout') && !wp_doing_ajax()) {
					wp_redirect('/my-account');
					die();
				}
			}
		}
	}
	//prevent users from accessing shop page.
	if (get_permalink(get_page_by_path('shop')) ==  home_url($_SERVER['REQUEST_URI'])) {
		wp_redirect(home_url());
		die();
	}
}
add_action('init', 'huslas_redirect_users');


/**
 *  function adds script to force users to upload profile pictures
 *
 * @return void
 */
function huslas_hook_javascript_footer()
{


	$url_str = home_url($_SERVER['REQUEST_URI']);

	$regex = "/my-account/i";
	if (preg_match($regex, $url_str, $match)) {
		if (isset($_SESSION['om_response'])) {
			$om_response = $_SESSION['om_response'];
?>
			<script>
				(function($) {

					<?php if ($om_response['status']) { ?>
						$("<div class='alert alert-success'>" + "<?php echo $om_response['message'] ?>" + "</div>").insertBefore("#feature-req-bx");

						$("<div class='alert alert-info'>" + "<?php echo $om_response['display_message'] ?>" + "</div>").insertBefore("#feature-req-bx");

						$("#feature-req-bx>").hide();

					<?php } else { ?>
						$("<div class='alert alert-danger'>" + "<?php echo $om_response['message'] ?>" + "</div>").insertBefore("form.feature-payment-form");
					<?php } ?>

				})(jQuery);
			</script>
<?php
			unset($_SESSION['om_response']);
		}
	}
}

add_action('wp_footer', 'huslas_hook_javascript_footer');

function huslas_child_service_finder_theme_setup()
{
	if (!isset($redux_demo) && class_exists('ReduxFrameworkPlugin') && file_exists(get_template_directory() . '/lib/options.php')) {
		//load_theme_textdomain('service-finder', WP_PLUGIN_DIR . '/redux-framework/ReduxCore/languages');
		require_once(get_stylesheet_directory() . '/lib/option.php');
	}
}
add_action('after_setup_theme', 'huslas_child_service_finder_theme_setup', 40);

/**
 * function converts between currencies
 *
 * @param [integer] $amount
 * @param [string] $from
 * @param [string] $to
 * @return converted amount
 */
function anevCurrencyConverter($amount, $from, $to)
{
	$from_Currency = urlencode($from);
	$to_Currency = urlencode($to);
	$query =  "{$from_Currency}_{$to_Currency}";
	$json = file_get_contents("https://free.currconv.com/api/v6/convert?q={$query}&compact=ultra&apiKey=86f4f975029a9cf1bbed");
	$obj = json_decode($json, true);
	$val = floatval($obj["$query"]);
	$total = $val * $amount;

	return floatval(number_format($total, 2, '.', ''));
}

/**
 * function redirects users to my-account page after they delete
 * all cart items
 *
 * @param [string] $link
 * @return $link
 */
function hj_redirect_users_empty_cart($link)
{
	$link =  get_permalink(get_page_by_path('my-account'));
	return $link;
}
add_filter('woocommerce_return_to_shop_redirect', 'hj_redirect_users_empty_cart');

/**
 * function change return button text on empty cart page
 *
 * @param [string] $button_text
 * @return $button_text
 */
function hj_change_text_empty_cart($button_text)
{
	$button_text = _e('Return to My Account', 'woocommerce');
	return $button_text;
}
add_filter('woocommerce_return_to_shop_text', 'hj_change_text_empty_cart');


function hj_provider_capabilities($cap_fields)
{

	unset($cap_fields['job-alerts']);
	unset($cap_fields['apply-for-job']);
	return $cap_fields;
}

add_filter('service_finder_provider_capabilities', 'hj_provider_capabilities');




// remove additional note fild on the checkout page
add_filter('woocommerce_enable_order_notes_field', '__return_false');


function huslajobs_reomove_checkout_fields($fields)
{

	unset($fields['billing']['billing_address_1']);
	unset($fields['billing']['billing_city']);
	unset($fields['billing']['billing_postcode']);
	unset($fields['billing']['billing_country']);

	return $fields;
}
add_filter('woocommerce_checkout_fields', 'huslajobs_reomove_checkout_fields');


function wc_auto_complete_paid_order($status, $order_id, $order)
{
	return 'completed';
}
add_action('woocommerce_payment_complete_order_status', 'wc_auto_complete_paid_order', 10, 3);
