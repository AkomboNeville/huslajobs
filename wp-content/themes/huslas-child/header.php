<?php
ob_start();
$service_finder_ThemeParams = service_finder_global_params('service_finder_ThemeParams');
$service_finder_options = get_option('service_finder_options');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<!--[if IE 7 ]>  <html class="isie ie7 oldie no-js" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>  <html class="isie ie8 oldie no-js" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>  <html class="isie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
	<!-- Favicons Icon -->
	<?php service_finder_fav_icon(); //Favicon
	?>
	<?php wp_head(); ?>


	<script>
		$ = jQuery;
		$(document).ready(function() {
			$(".nav-menu").prepend("<div class='cross_button afclr'><a><i class='fa fa-times' aria-hidden='true'></i></a></div>");
			$(".cross_button").click(function() {
				$(this).parent(".nav-menu").removeClass("state-active");
				$(".overlay").removeClass("active");
				$(this).closest(".body_shift").parent().removeClass("no_overflow");
			});
		});
		$(document).ready(function(e) {
			$('#goog-gt-tt').html('');
		});
		$(window).load(function(e) {
			$('#goog-gt-tt').html('');
		});
	</script>





</head>

<body <?php body_class(); ?>>
	<?php
	$registerErrors = service_finder_global_params('registerErrors');
	$bookingcompleted = (isset($_GET['bookingcompleted'])) ? esc_html($_GET['bookingcompleted']) : '';
	$invoicepaidcompleted = (isset($_GET['invoicepaidcompleted'])) ? esc_html($_GET['invoicepaidcompleted']) : '';
	$upgrade = (isset($_GET['upgrade'])) ? esc_html($_GET['upgrade']) : '';
	$cancelsubscription = (isset($_GET['cancelsubscription'])) ? esc_html($_GET['cancelsubscription']) : '';
	$cancelfeatured = (isset($_GET['cancelfeatured'])) ? esc_html($_GET['cancelfeatured']) : '';
	$featured = (isset($_GET['featured'])) ? esc_html($_GET['featured']) : '';
	$created = (isset($_GET['created'])) ? esc_html($_GET['created']) : '';
	$signup = (isset($_GET['signup'])) ? esc_html($_GET['signup']) : '';
	$joblimitplanupdate = (isset($_GET['joblimitplanupdate'])) ? esc_html($_GET['joblimitplanupdate']) : '';
	$walletpaymentupdate = (isset($_GET['walletpaymentupdate'])) ? esc_html($_GET['walletpaymentupdate']) : '';
	$registration = (isset($_GET['registration'])) ? esc_html($_GET['registration']) : '';
	$cancel_membership_id = (isset($_GET['cancel_membership_id'])) ? esc_html($_GET['cancel_membership_id']) : 0;
	$allowedhtml = array(
		'div' => array(
			'class' => array(),
		),
	);
	if (is_wp_error($registerErrors)) {
		echo '<div class="alert text-center alert-danger sf-error-top"><strong>' . $registerErrors->get_error_message() . '</strong></div>';
	}
	if ($registration == 'cancelled') {

		echo '<div class="alert text-center alert-danger sf-success-top"><strong>' . esc_html__("You cancelled payment. Your account wasn't created", "service-finder") . '</strong></div>';
	}
	$checkpae = service_finder_get_id_by_shortcodefortheme('[service_finder_thank_you]');
	if ($bookingcompleted == 'success' && (!is_page(service_finder_get_id_by_shortcodefortheme('[service_finder_thank_you]')) || empty($checkpae))) {

		$msg = (!empty($service_finder_options['provider-booked'])) ? $service_finder_options['provider-booked'] : esc_html__('Booking made successfully.', 'service-finder');
		wp_redirect('https://huslajobs.com/my-account/?action=bookings');

		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($invoicepaidcompleted == 'success' && (!is_page(service_finder_get_id_by_shortcodefortheme('[service_finder_thank_you]')) || empty($checkpae))) {
		$msg = (!empty($service_finder_options['pay-invoice'])) ? $service_finder_options['pay-invoice'] : esc_html__('Invoice paid successfully', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($upgrade == 'success') {
		$msg = (!empty($service_finder_options['provider-upgrade-successfull'])) ? $service_finder_options['provider-upgrade-successfull'] : esc_html__('Your account has been upgraded succssfully', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($upgrade == 'failed') {
		$msg = (!empty($service_finder_options['provider-upgrade-failed'])) ? $service_finder_options['provider-upgrade-failed'] : esc_html__('Your account upgrade failed.', 'service-finder');
		echo '<div class="alert text-center alert-danger sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($upgrade == 'pending') {
		$msg = (!empty($service_finder_options['provider-upgrade-pending'])) ? $service_finder_options['provider-upgrade-pending'] : esc_html__('Payment via wire transfer is pending for package upgrade', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($cancelsubscription == 'success') {
		$msg = (!empty($service_finder_options['subscription-cancel'])) ? $service_finder_options['subscription-cancel'] : esc_html__('Subscription Cancelled successfully.', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($cancelfeatured == 'success') {
		$msg = (!empty($service_finder_options['featured-cancel'])) ? $service_finder_options['featured-cancel'] : esc_html__('Featured/Featured Request Cancelled successfully.', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($featured == 'success') {
		$user_id = get_current_user_id();
		$user_info = get_userdata($user_id);
		$user_email = $user_info->user_email;
		$admin_email = get_option('admin_email');
		//Admin mail for profile featured

		wp_mail($admin_email, 'Huslas profile featured', 'Hello Admin, <br> User Name: ' . $user_info->user_login . ', <br>Have featured profile now.');

		//User mail for profile featured
		wp_mail($user_email, 'Huslas profile featured', 'Hello ' . $user_info->user_login . ',<br> Your profile is featured now.');

		$msg = (!empty($service_finder_options['feature-payment'])) ? $service_finder_options['feature-payment'] : esc_html__('Your profile is featured now', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($featured == 'wired') {
		$msg = (!empty($service_finder_options['feature-wiretransfer'])) ? $service_finder_options['feature-wiretransfer'] : esc_html__('You paid via wire transfer. Please wait for approval.', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($created == 'success') {
		$msg = (!empty($service_finder_options['provider-signup-successfull'])) ? $service_finder_options['provider-signup-successfull'] : esc_html__('Congratulations, your account has been created and you are now a Husla. Please check your emails / spam and click on the link to verify your email address in order to proceed with login.', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($signup == 'success') {
		$msg = (!empty($service_finder_options['customer-signup-successfull'])) ? $service_finder_options['customer-signup-successfull'] : esc_html__('Your account has been created,  Please verify your email ID for login. Check mail / spam.', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($joblimitplanupdate == 'success') {
		$msg = (!empty($service_finder_options['jobapply-limit-successfull'])) ? $service_finder_options['jobapply-limit-successfull'] : esc_html__('Your  plan updated succssfully', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($walletpaymentupdate == 'success') {
		$msg = (!empty($service_finder_options['update-wallet-amount-successfull'])) ? $service_finder_options['update-wallet-amount-successfull'] : esc_html__('Amount has been added to wallet successfully', 'service-finder');
	} elseif ($joblimitplanupdate == 'wired') {
		$msg = (!empty($service_finder_options['jobapply-limit-wiretransfer'])) ? $service_finder_options['jobapply-limit-wiretransfer'] : esc_html__('You paid via wire transfer. Please wait for approval.', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($walletpaymentupdate == 'wired') {
		$msg = (!empty($service_finder_options['update-wallet-amount-wiretransfer'])) ? $service_finder_options['update-wallet-amount-wiretransfer'] : esc_html__('You paid via wire transfer. Please wait for approval.', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($invoicepaidcompleted == 'wired') {
		$msg = (!empty($service_finder_options['invoice-pay-wiretransfer'])) ? $service_finder_options['invoice-pay-wiretransfer'] : esc_html__('You paid via wire transfer. Please wait for approval.', 'service-finder');
		echo '<div class="alert text-center alert-success sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
	} elseif ($cancel_membership_id > 0) {
		if (service_finder_check_provider_membership_status($cancel_membership_id) == 'draft') {
			$msg = esc_html__('Your membership has been cancelled.', 'service-finder');
			echo '<div class="alert text-center alert-danger sf-success-top"><strong>' . esc_html($msg) . '</strong></div>';
		}
	}
	?>
	<?php
	$custom_logo_id = get_theme_mod('custom_logo');
	$image = wp_get_attachment_image_src($custom_logo_id, 'full');
	$email = get_field('email', 'options');
	$current_user = wp_get_current_user();
	//print_r($current_user);
	$u_name = $current_user->display_name;
	$first_name = $current_user->first_name;
	?>

	<div class="page-wraper page_wraper_new">
		<!-- header -->
		<div class="header_top_section afclr">
			<div class="wrapper">
				<div class="header_top_se_inner afclr">
					<div class="h_top_se_left">
						<a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope"></i><?php echo $email; ?></a>
					</div>
					<div class="h_top_se_right">
						<div class="h_top_se_right_inner afclr">
							<div class="h_top_language afclr">
								<div class="h_top_language_inner afclr">
									<?php echo do_shortcode('[gtranslate]'); ?>
								</div>
							</div>
							<div class="h_top_login">
								<?php
								if (!empty($u_name)) { ?>
									<a href="<?php echo get_site_url(); ?>/my-account">My Account</a>
									<a href="<?php echo wp_logout_url(home_url('/')); ?>">Logout</a>
								<?php } else { ?>
									<a href="<?php echo get_permalink(33); ?>">Login</a>
								<?php } ?>
							</div>

						</div>

					</div>


				</div>
			</div>

		</div>
		<div class="header_section afclr">
			<div class="wrapper afclr">
				<div class="logo"><a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt=""></a></div>
				<div class="top_right">
					<div class="site-menu afclr">
						<a href="javascript:void(1)" class="menu_expand afclr">
							<i></i>
							<i></i>
							<i></i>
						</a>
						<?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'header_menu')); ?>

					</div>
					<div class="h_right_becum_btn">
						<?php
						if (!empty($u_name)) { ?>
							<a>Hello, <?php echo $first_name; ?></a>
						<?php } else { ?>
							<a href="<?php echo get_permalink(32); ?>"><i class="fa fa-bookmark"></i> Become a Husla</a>
						<?php } ?>
					</div>
				</div>



			</div>


		</div>

		<?php
		if (in_array(get_the_ID(), service_finder_get_id_by_shortcodefortheme('[service_finder_my_account'))) {
			service_finder_myaccount_layout();
		} else {
		}
		?>
		<!-- header END -->