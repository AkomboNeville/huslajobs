<?php get_header(); ?>
<h1 style="display:none;">Platform for service providers</h1>
 <div class="banner_section afclr">
 <?php 
$banner_image = get_field('banner_image');
if( !empty( $banner_image ) ): ?>
    <img src="<?php echo esc_url($banner_image['url']); ?>" alt="<?php echo esc_attr($banner_image['alt']); ?>" />
<?php endif; ?>
 		<div class="banner_pos_section afclr">
 <div class="wrapper afclr">
 <div class="banner_se_inner afclr">
 <h2><?php the_field('banner_heading');?></h2>
<?php the_field('banner_content');?>
<div class="h_banner_search_bar h_banner_search_bl_desktop afclr">
<div class="h_banner_search_block afclr">
<?php /*?><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/search_bar_img.jpg" alt="search_bar_img"><?php */?>
<div class="sf-find-bar sf-search-result pos-v-bottom">
 <div class="search-form <?php echo sanitize_html_class($advanceclass); ?>"> 
        <?php echo do_shortcode('[service_finder_search_form]'); ?>
      </div>
 </div>
      
</div>
</div>

 </div>
 </div>
</div> 


 </div>
 
<div class="h_banner_search_bar h_banner_search_bl_mobile afclr">
<div class="wrapper">
<div class="h_banner_search_block afclr">
<div class="sf-find-bar sf-search-result pos-v-bottom">
 <div class="search-form <?php echo sanitize_html_class($advanceclass); ?>"> 
        <?php echo do_shortcode('[service_finder_search_form]'); ?>
      </div>
 </div>
      
</div>
</div>
</div>
 
 <div class="h_service_section afclr">
 <div class="wrapper">
 <div class="h_serv_title afclr"><h2>Our Services</h2></div>
  <div class="h_service_se_inner afclr">
  <div class="h_service_ad_left">
  <div class="h_service_ad_left_inner afclr">
  <p><?php the_field('ad1_content');?></p>
<?php 
$ad1_image = get_field('ad1_image');
if( !empty( $ad1_image ) ): ?>
    <img src="<?php echo esc_url($ad1_image['url']); ?>" alt="<?php echo esc_attr($ad1_image['alt']); ?>" />
<?php endif; ?>
  </div>
  </div>
  <div class="h_service_se_block">
  <div class="h_service_se_bl_inner afclr">
 
 <?php $terms = get_terms(
    array(
        'taxonomy'   => 'providers-category',
        'hide_empty' => false,
		'parent'=>0
    )
);
$i=0;
if ( ! empty( $terms ) && is_array( $terms ) ) {
	

    foreach ( $terms as $term ) { 
	if($i<16){
	$catimage =  service_finder_getCategoryIcon($term->term_id,'service_finder-all-category-icon');
	?> 
  <div class="h_service_block">
  <div class="h_service_block_inner afclr">
  <div class="h_service_bl_img afclr"><a href="<?php echo esc_url( get_term_link( $term ) ) ?>"><img src="<?php echo $catimage; ?>" alt="Catering image" /></a></div>
  <div class="h_service_bl_content afclr">
  <h3><a href="<?php echo esc_url( get_term_link( $term ) ) ?>"><?php echo $term->name; ?></a></h3>
  </div>
  </div>
  </div>
  
  <?php   $i++;  }}
} 
	 
	 ?>
  
  </div>
  <div class="h_service_view_btn afclr">
  <?php 
$s_link = get_field('services_view_all_button_link');
if( $s_link ): 
    $link_url = $s_link['url'];
    $link_title = $s_link['title'];
    $link_target = $s_link['target'] ? $s_link['target'] : '_self';
    ?>
    <a class="h_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
<?php endif; ?>
  
  </div>
  </div>
  
  
  <div class="h_service_ad_right">
   <div class="h_service_ad_left_inner afclr">
  <p><?php the_field('ad2_content');?></p>
  <?php 
$ad2_image = get_field('ad2_image');
if( !empty( $ad2_image ) ): ?>
    <img src="<?php echo esc_url($ad2_image['url']); ?>" alt="<?php echo esc_attr($ad2_image['alt']); ?>" />
<?php endif; ?>
  </div>
  </div>
  
  </div>
 </div>
 </div>
 
 <?php global $wpdb;
    $table_name = 'service_finder_feature';
    $count_query = "select count(*) from $table_name";
    $finder_feature_atatus = $wpdb->get_var($count_query);
if($finder_feature_atatus >0){
?>

<div class="h_featured_section afclr">
<div class="wrapper">
<div class="h_serv_title afclr"><h2>Featured Providers</h2></div>
<?php /*?><div class="h_featured_se_inner afclr">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/featured_section_img.jpg" alt="featured section img">

</div><?php */?>
</div>
<div class="h_featured_se_inner afclr">
<?php echo do_shortcode('[Featured-Providers title="" showitem="3" numberofproviders="10" fullwidth="yes"]');?>
</div>
</div>
<?php } ?>

<div class="h_newslatter_section afclr">
<div class="wrapper">
<div class="h_newslatter_se_inner afclr">
<div class="h_newsletter_left">
<div class="ah_newsletter_l_img afclr"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/newsletter_img.png" alt="newsletter"></div>
<div class="ah_newsletter_l_title afclr"><h3>SIGN UP FOR NEWSLETTER:</h3></div>
</div>
<div class="h_newsletter_right">
<div class="h_newsletter_ri_inner afclr">
<?php /*?><input class="h_new_la_inp" type="email" placeholder="Enter your email address">
<input class="h_new_la_sbmt" type="submit" value="Signup Now"><?php */?>


<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<div id="mc_embed_signup">
<form action="https://huslajobs.us4.list-manage.com/subscribe/post?u=84704e39bc121f579315574ac&amp;id=11170784c6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
    <div id="mc_embed_signup_scroll">
	<input type="email" value="" name="EMAIL" class="required email h_new_la_inp" id="mce-EMAIL" placeholder="Enter your email address">
    <input type="submit" value="Signup Now" name="subscribe" id="mc-embedded-subscribe" class="h_new_la_sbmt">
</div>
	<div id="mce-responses" class="">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_84704e39bc121f579315574ac_11170784c6" tabindex="-1" value=""></div>
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->

</div>

</div>

</div>
</div>
</div>

<div class="h_latest_blog_section afclr">
<div class="wrapper">
<div class="h_serv_title afclr"><h2>Latest Blog Post</h2></div>
<div class="h_latest_blog_se_inner afclr">
 <div class="swiper-container swiper_blog">
    <div class="swiper-wrapper">
 <?php $args22 = array(
'posts_per_page'   => 10,
'offset'           => 0,
'category'         => '',
'orderby'          => 'post_date',
'order'            => 'DESC',
'include'          => '',
'exclude'          => '',
'meta_key'         => '',
'meta_value'       => '',
'post_type'        => 'post',
'post_mime_type'   => '',
'post_parent'      => '',
'post_status'      => 'publish',
'suppress_filters' => true ,
);

 ?>
<?php $posts_array22 = get_posts( $args22 ); ?> 
<?php foreach($posts_array22 as $post22)
 {?>
<div class="swiper-slide">
<div class="h_l_blog_block">
<div class="h_l_blog_bl_inner afclr">
<div class="h_l_blog_bl_img afclr"><a href="<?php echo get_permalink($post22->ID); ?>"><?php echo get_the_post_thumbnail( $post22->ID, 'home_post' ); ?></a></div>
<div class="h_l_blog_bl_content afclr">
<h3><a href="<?php echo get_permalink($post22->ID); ?>"><?php echo $post22->post_title; ?></a></h3>
<div class="h_blog_author afclr"><span><i class="fa fa-user"></i> By <?php echo get_field('post_author_name', $post22->ID); ?></span> 
<?php /*?><span><i class="fa fa-comments"></i> Comments </span><?php */?>
</div>
<p><?php echo wp_trim_words( $post22->post_content,15) ?></p>
</div>
</div>
</div>
</div>
<?php } ?>

</div>
</div>
 <div class="swiper-button-next swiper-button-white swiper_blog_next"></div>
 <div class="swiper-button-prev swiper-button-white swiper_blog_prev"></div>
  

</div>

</div>
</div>

<div class="h_testmonial_section afclr">
<div class="wrapper">
<div class="h_serv_title afclr"><h2>Testimonials</h2></div>
<div class="h_testmonial_se_inner afclr">
<div class="swiper-container swipper_testmonial">
    <div class="swiper-wrapper">
     <?php
if( have_rows('testimonials_section') ): ?>
 	<?php // loop through the rows of data
        while ( have_rows('testimonials_section') ) : the_row(); ?>
    <?php $testimonials_image = get_sub_field('testimonials_image');
	$testimonials_title = get_sub_field('testimonials_title');
	$testimonials_position = get_sub_field('testimonials_position');
	$testimonials_content = get_sub_field('testimonials_content');
	?> 
      <div class="swiper-slide">
		<div class="h_testmonial_se_slider afclr">
<div class="h_testmon_left">
<div class="h_testmon_left_img afclr"><img src="<?php echo $testimonials_image['url']; ?>" alt="<?php echo esc_attr($testimonials_image['alt']); ?>" /></div>
<div class="h_testmon_left_contnt afclr"><h4><?php echo $testimonials_title; ?></h4><h3><?php echo $testimonials_position; ?></h3></div>
</div>
<div class="h_testmon_right">
<div class="h_testmon_right_inner afclr">
<div class="h_testml_quot afclr"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonail_quote_img.png" alt="testimonail_quote"></div>
<?php echo $testimonials_content; ?>

</div>
</div>

</div>
	  </div>
       <?php endwhile; ?>
 <?php endif; ?> 
      
     </div>
     <div class="h_testmnl_arrow afclr">
     <div class="swiper-button-next swiper_testmnl_next"></div>
    <div class="swiper-button-prev swiper_testmnl_prev"></div>
    </div>
</div>

 
</div>

</div>
</div>
	
<?php get_footer();?>


