<?php
$service_finder_options = get_option('service_finder_options');
$service_finder_ThemeParams = service_finder_global_params('service_finder_ThemeParams');
?>
<?php $email = get_field('email', 'options');
$footer_text = get_field('footer_text', 'options');
$facebook_link = get_field('facebook_link', 'options');
$twitter_link = get_field('twitter_link', 'options');
$google_link = get_field('google_link', 'options');
$instagram_link = get_field('instagram_link', 'options');
$linked_link = get_field('linked_link', 'options');
$footer_copyright = get_field('footer_copyright', 'options');

?>
<!-- Footer -->
<div class="f_footer_section afclr">
  <div class="wrapper">
    <div class="f_footer_se_inner afclr">
      <div class="f_footer_left">
        <div class="f_footer_left_iner afclr">
          <div class="f_footer_logo afclr"><a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="Logo"></a></div>
          <div class="f_footer_l_content afclr">
            <p><?php echo $footer_text; ?>
            </p>
          </div>
          <div class="f_footer_social afclr">
            <h3>Follow Us:</h3>
            <div class="f_footer_s_media afclr">
              <?php if (!empty($facebook_link)) { ?>
                <a href="<?php echo $facebook_link; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
              <?php }
              if (!empty($twitter_link)) {
              ?>
                <a href="<?php echo $twitter_link; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
              <?php }
              if (!empty($google_link)) {
              ?>
                <a href="<?php echo $google_link; ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
              <?php }
              if (!empty($instagram_link)) {
              ?>
                <a href="<?php echo $instagram_link; ?>" target="_blank"><i class="fa fa-instagram"></i></a>
              <?php }
              if (!empty($linked_link)) {
              ?>
                <a href="<?php echo $linked_link; ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
              <?php }  ?>
            </div>

          </div>
          <div class="f_footer_menu afclr">
            <div class="f_footer_abslt afclr"></div>
            <?php wp_nav_menu(array('theme_location' => 'footer_menu', 'menu_class' => '', 'menu_id' => '')); ?>
          </div>

        </div>
      </div>
      <div class="f_footer_right">
        <div class="f_footer_right_inner afclr">
          <h2>Search by Category</h2>
          <div class="f_footer_service_menu afclr">
            <?php wp_nav_menu(array('theme_location' => 'footer_category_menu', 'menu_class' => '', 'menu_id' => '')); ?>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="f_copy_section afclr">
  <div class="wrapper">
    <div class="f_copy_inner afclr">
      <p><?php echo $footer_copyright; ?></p>
    </div>
    <?php
    $user_id = get_current_user_id();
    $user = wp_get_current_user(); // getting & setting the current user

    ?>

  </div>

</div>



</div>
<?php
wp_add_inline_script('image-manager', 'var rwmbFile = {"maxFileUploadsSingle":"' . esc_html__('You may only upload maximum %d file', 'service-finder') . '","maxFileUploadsPlural":"' . esc_html__('You may only upload maximum %d files', 'service-finder') . '"};
var RWMB = {"url":"' . esc_html($service_finder_ThemeParams['themeImgUrl']) . '"};', 'before');

wp_add_inline_script('managefiles', 'var rwmbFile = {"maxFileUploadsSingle":"' . esc_html__('You may only upload maximum %d file', 'service-finder') . '","maxFileUploadsPlural":"' . esc_html__('You may only upload maximum %d files', 'service-finder') . '"};
var RWMB = {"url":"' . esc_html($service_finder_ThemeParams['themeImgUrl']) . '"};', 'before');

?>


<!-- Loading area start -->
<div class="loading-area default-hidden">
  <div class="loading-box"></div>
  <div class="loading-pic"></div>
</div>
<!-- Loading area end -->

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/site_functions.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.fancybox.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/intlTelInput.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/swiper.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.matchHeight-min.js"></script>

<script>
  $ = jQuery;
  $(document).ready(function() {
    $(function() {
      $('.h_service_bl_content h3').matchHeight();
      $('.sn_signup_left').matchHeight();
      $('.sn_signup_le_inner p').matchHeight();
    });

    $(window).load(function(e) {
      $('.h_service_bl_content h3').matchHeight();

      $('.sn_signup_left').matchHeight();
      $('.sn_signup_le_inner p').matchHeight();
    });
  });
</script>
<script>
  $ = jQuery;
  $(document).ready(function() {
    var swiper = new Swiper('.swiper_blog', {
      slidesPerView: 3,
      loop: true,
      spaceBetween: 20,
      speed: 1000,
      autoplay: {
        delay: 5000,


        disableOnInteraction: false,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {

        992: {
          slidesPerView: 3,
          spaceBetween: 10,
        },

        767: {
          slidesPerView: 2,
          spaceBetween: 0,
        },

        640: {
          slidesPerView: 2,
          spaceBetween: 0,

        },

        480: {

          slidesPerView: 1,
          spaceBetween: 0,

        }

      }
    });


    var swiper = new Swiper('.swipper_testmonial', {
      autoplay: {
        delay: 5000,
      },
      navigation: {
        nextEl: '.swiper_testmnl_next',
        prevEl: '.swiper_testmnl_prev',
      },
    });

  });
  $(document).ready(function() {
    $("#hidden_link").fancybox({
      closeBtn: false,
      closeClick: false,
      modal: true,
      helpers: {
        overlay: {
          closeClick: false
        }
      },
      keys: {
        close: null
      }

    }).trigger('click');

    $('.select_dtype input[name="provider_role"]').change(function() {
      var package_val = $(this).val();
      $('.select_plan_drop').val(package_val);
      // alert($(this).val());
    });

    $('.popup_for_not_signup a').click(function(e) {
      window.location.href = '<?php echo get_permalink(32); ?>';
    });


  });
</script>
<script>
  <?php if (is_page(2216)) { ?>
    var input = document.querySelector("#signup_phone");

    var iti = window.intlTelInput(input, {
      nationalMode: true,

      utilsScript: "<?php echo get_stylesheet_directory_uri(); ?>/js/utils.js?1562189064761" // just for formatting/placeholders etc
    });
    intlTelInput(input, {
      initialCountry: "auto",
      geoIpLookup: function(success, failure) {
        $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : "";
          success(countryCode);
          iti.setCountry(countryCode);
        });
      },
    });
  <?php }
  if (is_page(34)) {
  ?>

    var input = document.querySelector("#p_profile_phone1");
    var iti = window.intlTelInput(input, {
      nationalMode: true,

      utilsScript: "<?php echo get_stylesheet_directory_uri(); ?>/js/utils.js?1562189064761" // just for formatting/placeholders etc
    });
    intlTelInput(input, {
      initialCountry: "auto",
      geoIpLookup: function(success, failure) {
        $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : "";
          success(countryCode);
          iti.setCountry(countryCode);

        });
      },
    });
  <?php } ?>


  $(document).ready(function() {
    $.fn.bootstrapValidator.validators.password = {
      validate: function(validator, $field, options) {
        var value = $field.val();
        if (value === '') {
          return true;
        }

        if (value.length < 8) {
          return false;
        }
        if (value === value.toLowerCase()) {
          return false;
        }
        if (value === value.toUpperCase()) {
          return false;
        }
        if (value.search(/[0-9]/) < 0) {
          return false;
        }

        return true;
      }
    };

  });
</script>



<?php wp_footer(); ?>
<?php

$userInfo = service_finder_getCurrentUserInfo();
$url_str = home_url($_SERVER['REQUEST_URI']);
$tabname = (isset($_GET['tabname'])) ? esc_html($_GET['tabname']) : '';

$regex = "/my-account/i";
if (preg_match($regex, $url_str, $match)) {

  $manageaccountby = (isset($_GET['manageaccountby'])) ? esc_attr($_GET['manageaccountby']) : '';
  $manageproviderid = (isset($_GET['manageproviderid'])) ? esc_attr($_GET['manageproviderid']) : '';
  $current_user = service_finder_plugin_global_vars('current_user');

  if (service_finder_getUserRole($current_user->ID) == 'Provider' || service_finder_check_account_authorization($manageaccountby, $manageproviderid)) {

    if (service_finder_getUserRole($current_user->ID) == 'Provider') {

      $userInfo = service_finder_getCurrentUserInfo();
    } else {

      $userInfo = service_finder_getUserInfo($manageproviderid);
    }
  }


  $script = '    <script defer>
            (function($) {

                "use strict";



                $(document).ready(function () {';
  $user_profile_image = service_finder_get_user_profile_image($userInfo['avatar_id']);
  if (!$user_profile_image) {
    $profile_notice = esc_html__('Please upload  your profile picture ', 'service-finder');
    $script .=  ' $("#myTab a").off("click");
    $(".sf-page-title").after("<div class=\"alert alert-danger\">Welcome back, Please upload your profile picture, to fully use the site like before </div>");


    ';
  }
  $script .= '
               $("#myTab a[href = #my-profile]").each(function(index,element){
                    if(index== 0){
                      $(this).attr("id","my-profile-setting");
                    }

                });

               	//Tabbing on My Account Page
                	$("#myTab a").on("click",function(e){
                    e.preventDefault();
                    console.log($(this).attr("href"));
                    $("#myTab li").removeClass("active");
                     var tabid = $(this).attr("href");
                    ';
  $user_profile_image = service_finder_get_user_profile_image($userInfo['avatar_id']);
  if (!$user_profile_image) {

    $script .=  '
                 $(this).tab("show");
                 var tabSectionId = tabid.split("#")[1];
                 if(tabSectionId !="my-profile"){
                   var selector = "#"+tabSectionId;

                    $(selector).html("<div class=\"alert alert-danger\"> Please upload your profile picture, to proceed. Go to <a href=\"/my-account\" style=\" text-decoration:underline\">Profile Settings</div>");
                 }


              // setTimeout(function(){ $(this).tab("show") }, 10000);
              // $("#myTab a#my-profile-setting").tab("show");
              //  $("<div class=\"alert alert-danger\"> Please upload your profile picture, to proceed </div>").insertBefore(".sf-page-title");
';
  } else {

    $script .= '   $(this).tab("show")';
  }


  $script .= '


                		if(service_finder_getCookie("tabid") != tabid && tabid != "#schedule"){
                			$(".loading-area").show();
                		}

                		service_finder_setCookie("tabid", tabid);';

  $script .= '    	});
            });
            })(jQuery);
        </script>';

  echo $script;
}


?>
</body>

</html>