<?php
/**
 Template name: Services List category template
 */

get_header(); ?>

<div class="h_service_section afclr">
 <div class="wrapper">
 <div class="h_serv_title afclr"><h2><?php the_title();?></h2></div>
  <div class="h_service_se_inner afclr">
  <div class="h_service_ad_left">
  <div class="h_service_ad_left_inner afclr">
  <p><?php the_field('ad1_content', 1776);?></p>
<?php 
$ad1_image = get_field('ad1_image', 1776);
if( !empty( $ad1_image ) ): ?>
    <img src="<?php echo esc_url($ad1_image['url']); ?>" alt="<?php echo esc_attr($ad1_image['alt']); ?>" />
<?php endif; ?>
  </div>
  </div>
  <div class="h_service_se_block">
  <div class="h_service_se_bl_inner afclr">
 
 <?php $terms = get_terms(
    array(
        'taxonomy'   => 'providers-category',
        'hide_empty' => false,
    )
);

if ( ! empty( $terms ) && is_array( $terms ) ) {
 foreach ( $terms as $term ) { 
	$catimage =  service_finder_getCategoryIcon($term->term_id,'service_finder-all-category-icon');
	?> 
  <div class="h_service_block">
  <div class="h_service_block_inner afclr">
  <div class="h_service_bl_img afclr"><a href="<?php echo esc_url( get_term_link( $term ) ) ?>"><img src="<?php echo $catimage; ?>" alt="" /></a></div>
  <div class="h_service_bl_content afclr">
  <h3><a href="<?php echo esc_url( get_term_link( $term ) ) ?>"><?php echo $term->name; ?></a></h3>
  </div>
  </div>
  </div>
  
  <?php   }
} 
	 
	 ?>
  
  </div>
  
  </div>
  
  
  <div class="h_service_ad_right">
   <div class="h_service_ad_left_inner afclr">
  <p><?php the_field('ad2_content', 1776);?></p>
  <?php 
$ad2_image = get_field('ad2_image', 1776);
if( !empty( $ad2_image ) ): ?>
    <img src="<?php echo esc_url($ad2_image['url']); ?>" alt="<?php echo esc_attr($ad2_image['alt']); ?>" />
<?php endif; ?>
  </div>
  </div>
  
  </div>
 </div>
 </div>


<?php get_footer(); ?>