<?php


// No direct access

if (!defined('ABSPATH')) exit;


/**
 * CU = customer upgrade
 */
if (!class_exists('CU_Woopayments') && class_exists('WooCommerce')) {



	class CU_Woopayments

	{

		private $checkout_info = array();

		private $product_id;

		private $orderid;

		private $wooaction;



		public function __construct()

		{

			global $service_finder_options;

			$woopayment = (isset($service_finder_options['woocommerce-payment'])) ? esc_html($service_finder_options['woocommerce-payment']) : false;

			$this->product_id = (isset($service_finder_options['woo-product-id'])) ? esc_html($service_finder_options['woo-product-id']) : '';

			add_action('wp_ajax_hj_add_to_woo_cart', array($this, 'huslajobs_add_to_woo_cart'));

			add_action('wp_ajax_nopriv_hj_add_to_woo_cart', array($this, 'huslajobs_add_to_woo_cart'));


			if ($woopayment) {

				// order meta
				// unset($fields['billing']['billing_company']);

				$user = wp_get_current_user();

				if (!empty($user->roles) && is_array($user->roles)) {

					foreach ($user->roles as $role) {

						if ($role == 'Customer') {

							add_action('woocommerce_add_order_item_meta',      array($this, 'add_booking_meda_data'), 10, 3);

							add_action('woocommerce_after_order_itemmeta',     array($this, 'booking_meda_data'), 10, 1);

							add_action('woocommerce_before_calculate_totals',  array($this, 'before_calculate_price'), 10, 1);

							add_action('woocommerce_order_item_meta_end',      array($this, 'booking_meda_data'), 10, 1);

							add_action('woocommerce_checkout_update_customer', array($this, 'custom_checkout_update_customer'), 10, 2);


							//order status
							add_action('woocommerce_order_status_cancelled',   array($this, 'cancelOrder'), 10, 1);

							add_action('woocommerce_order_status_completed',   array($this, 'paymentComplete'), 10, 1);

							add_action('woocommerce_order_status_on-hold',     array($this, 'paymentComplete'), 10, 1);

							add_action('woocommerce_order_status_processing',  array($this, 'paymentComplete'), 10, 1);

							add_action('woocommerce_order_status_refunded',    array($this, 'cancelOrder'), 10, 1);

							add_action('woocommerce_deleted_order_items',    	array($this, 'deletedOrder'), 10, 1);


							// filters
							add_filter('woocommerce_get_item_data',            array($this, 'update_cart_meta_item'), 10, 2);

							add_filter('woocommerce_quantity_input_args',      array($this, 'remove_quantity_field'), 10, 2);
						}
					}
				}
			}
		}

		public function booking_meda_data($item_id)
		{

			$data = wc_get_order_item_meta($item_id, 'wooextradata');

			if ($data) {

				$other_data = $this->update_cart_meta_item(array(), array('wooextradata' => $data));

				if (!empty($other_data)) {

					echo '<br/>' . $other_data[0]['name'] . '<br/>' . nl2br($other_data[0]['value']);
				}
			}
		}

		public function update_cart_meta_item($other_data, $wc_item)
		{

			global $service_finder_options;

			if (isset($wc_item['wooextradata'])) {

				$wootype = $wc_item['wooextradata']['wootype'];

				switch ($wootype) {

					case 'customerupgrade':

						if (isset($wc_item['wooextradata'])) {

							$other_data[] = array('name' => esc_html__('Request', 'service-finder'), 'value' => $wc_item['wooextradata']['package_name']);
						}

						break;
				}
			}

			return $other_data;
		}

		public function paymentComplete($order_id)
		{

			global $wpdb;

			$order = new \WC_Order($order_id);

			$order_status = $order->get_status();



			if ($order->get_status() != 'failed') {

				foreach ($order->get_items() as $item_id => $order_item) {

					$wooextradata = wc_get_order_item_meta($item_id, 'wooextradata');

					$wootype = $wooextradata['wootype'];

					switch ($wootype) {

						case 'customerupgrade':

							$userId = $wooextradata['user_id'];
							$this->upgrade_after_payment($userId, $wooextradata, $order_id, $item_id);
							break;
					}
				}
			}
		}
		public function upgrade_after_payment($userId, $wooextradata, $order_id, $item_id)
		{

			global $service_finder_options, $wpdb, $service_finder_Tables;

			$order = new \WC_Order($order_id);


			$payment_method = $order->get_payment_method();

			$order_status = $order->get_status();



			if ($wooextradata && !isset($wooextradata['processed'])) {

				if ($order_status == 'on-hold' || $order_status == 'processing') {



					$wiredupgrade = array();

					$user = new WP_User($userId);

					$user->set_role('Customer');

					$userdata = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->users . ' WHERE `ID` = %d', $userId));

					$userInfo = service_finder_getUserInfo($userId);

					$expire_limit = $wooextradata['expire_limit'];

					$wiredupgrade['payment_type'] = 'woocommerce';

					$wiredupgrade['payment_mode'] = $payment_method;

					$wiredupgrade['price'] = $wooextradata['amount'];

					$wiredupgrade['time'] = time();

					$invoiceid = $order_id;

					$wiredupgrade['wired_invoiceid'] = $invoiceid;

					$wiredupgrade['recurring_profile_type'] = '';


					if ($expire_limit > 0) {

						$wiredupgrade['expire_limit'] = $expire_limit;
					}

					$userInfo = service_finder_getUserInfo($userId);

					$paymentstatus = 'Wire Transfer';

					update_user_meta($userId, 'upgrade_request', $wiredupgrade);

					update_user_meta($userId, 'upgrade_request_status', 'pending');

					update_user_meta($userId, 'order_id', $order_id);

					update_user_meta($userId, 'payment_mode', 'woocommerce');


					$primarycategory = get_user_meta($userId, 'primary_category', true);

					$args = array(

						'username' => $userdata->user_login,

						'email' => $userdata->user_email,

						'address' => $userInfo['address'],

						'city' => $userInfo['city'],

						'country' => $userInfo['country'],

						'zipcode' => $userInfo['zipcode'],

						'category' => service_finder_getCategoryNameviaSql($primarycategory),

						'package_name' => $wooextradata['package_name'],

						'payment_type' => $paymentstatus
					);
					service_finder_sendWiredUpgradeMailToProvider($userdata->user_login, $userdata->user_email, $args, $invoiceid);
					service_finder_sendProviderWiredUpgradeEmail($args, $invoiceid);
				} elseif ($order_status == 'completed') {
					$user = new WP_User($userId);
					$user->set_role('Customer');
					update_user_meta($userId, 'provider_activation_time', array('role' => 'customerupgrade', 'time' => time()));
					update_user_meta($userId, 'order_id', $order_id);
					update_user_meta($userId, 'expire_limit', $wooextradata['expire_limit']);
					update_user_meta($userId, 'profile_amt', $wooextradata['rolePrice']);
					update_user_meta($userId, 'pay_type', 'single');
					update_user_meta($userId, 'payment_type', 'woocommerce');
					update_user_meta($userId, 'payment_mode', $wooextradata['payment_mode']);
					update_user_meta($userId, 'upgrade_request_status', 'completed');
					$paymode = 'woocommerce';
					$userInfo = service_finder_getUserInfo($userId);
					$userdata = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->users . ' WHERE `ID` = %d', $userId));
					$username = $userdata->user_login;
					$useremail = $userdata->user_email;
					$args = array(
						'username' => (!empty($username)) ? $username : '',
						'email' => (!empty($useremail)) ? $useremail : '',
						'address' => (!empty($userInfo['address'])) ? $userInfo['address'] : '',
						'city' => (!empty($userInfo['city'])) ? $userInfo['city'] : '',
						'country' => (!empty($userInfo['country'])) ? $userInfo['country'] : '',
						'zipcode' => (!empty($userInfo['zipcode'])) ? $userInfo['zipcode'] : '',
						'category' => (!empty($userInfo['categoryname'])) ? $userInfo['categoryname'] : '',
						'package_name' => $wooextradata['package_name'],
						'payment_type' => $paymode
					);
					service_finder_sendProviderEmail($args);
					service_finder_sendRegMailToUser($username, $useremail);
				}
				$wooextradata['processed'] = true;
				wc_update_order_item_meta($item_id, 'wooextradata', $wooextradata);
				if ($order_status == 'completed') {
					$wooextradata['completed'] = true;
					wc_update_order_item_meta($item_id, 'wooextradata', $wooextradata);
				}
			} else {
				$row = $wpdb->get_row($wpdb->prepare('SELECT `user_id` FROM ' . $wpdb->prefix . 'usermeta WHERE `meta_value` = %d AND `meta_key` = "order_id"', $order_id));
				if (!empty($row)) {
					$requeststatus = get_user_meta($row->user_id, 'upgrade_request_status', true);
					if ($requeststatus == 'pending' && $order_status == 'completed' && $wooextradata && !isset($wooextradata['completed']) && !isset($wooextradata['cancelled'])) {
						$userId = $row->user_id;
						$requestdata = get_user_meta($userId, 'upgrade_request', true);
						update_user_meta($userId, 'payment_type', 'woocommerce');
						update_user_meta($userId, 'payment_mode', $requestdata['payment_mode']);
						update_user_meta($userId, 'wired_invoiceid', $requestdata['wired_invoiceid']);
						update_user_meta($userId, 'recurring_profile_type', $requestdata['recurring_profile_type']);
						if ($requestdata['expire_limit'] > 0) {
							update_user_meta($userId, 'expire_limit', $requestdata['expire_limit']);
						}
						update_user_meta($userId, 'provider_activation_time', array('role' => $wooextradata['package_name'], 'time' => time()));
						update_user_meta($userId, 'upgrade_request_status', 'completed');
						$email = service_finder_getProviderEmail($userId);
						$providerreplacestring = (!empty($service_finder_options['provider-replace-string'])) ? $service_finder_options['provider-replace-string'] : esc_html__('Customer', 'service-finder');
						if (!empty($service_finder_options['send-to-provider-upgrade-request-approval'])) {
							$message = $service_finder_options['send-to-provider-upgrade-request-approval'];
						} else {
							$message = 'Dear ' . esc_html($providerreplacestring) . ',
				Congratulations! Your account upgraded Successfully';
						}
						$msg_body = $message;
						if (!empty($service_finder_options['send-to-provider-upgrade-request-approval-subject'])) {
							$msg_subject = $service_finder_options['send-to-provider-upgrade-request-approval-subject'];
						} else {
							$msg_subject = 'Account Upgrade Notification';
						}
						service_finder_wpmailer($email, $msg_subject, $msg_body);
						$wooextradata['completed'] = true;
						wc_update_order_item_meta($item_id, 'wooextradata', $wooextradata);
					}
				}
			}
		}

		public function cancelOrder($order_id)
		{

			global $wpdb;
			$order = new \WC_Order($order_id);
			if ($order->get_status() != 'failed') {
				foreach ($order->get_items() as $item_id => $order_item) {
					$wooextradata = wc_get_order_item_meta($item_id, 'wooextradata');
					$wootype = $wooextradata['wootype'];
					switch ($wootype) {
						case 'customerupgrade':
							$this->upgrade_payment_cancel($wooextradata, $order_id, $item_id);

							break;
					}
				}
			}
		}

		public function upgrade_payment_cancel($wooextradata, $order_id, $item_id)
		{
			global $service_finder_options, $wpdb, $service_finder_Tables;
			$order = new \WC_Order($order_id);
			$payment_method = $order->get_payment_method();
			$order_status = $order->get_status();
			if ($wooextradata && !isset($wooextradata['completed'])) {
				$row = $wpdb->get_row($wpdb->prepare('SELECT `user_id` FROM ' . $wpdb->prefix . 'usermeta WHERE `meta_value` = %d AND `meta_key` = "order_id"', $order_id));
				if (!empty($row)) {
					$userId = $row->user_id;
					update_user_meta($userId, 'upgrade_request_status', 'cancelled');
					$wooextradata['cancelled'] = true;

					wc_update_order_item_meta($item_id, 'wooextradata', $wooextradata);
				}
			}
		}

		public function deletedOrder($order_id)
		{
			global $wpdb;
			$order = new \WC_Order($order_id);
			if ($order->get_status() != 'failed') {
				foreach ($order->get_items() as $item_id => $order_item) {

					$wooextradata = wc_get_order_item_meta($item_id, 'wooextradata');

					$wootype = $wooextradata['wootype'];

					switch ($wootype) {

						case 'customerupgrade':

							$this->delete_upgrade_request($order_id);

							break;

						default:

							break;
					}
				}
			}
		}

		public function delete_upgrade_request($order_id)
		{

			global $wpdb, $service_finder_Tables;

			$row = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'usermeta WHERE `meta_value` = %d AND `meta_key` = "order_id"', $order_id));

			if (!empty($row)) {

				$userid = $row->user_id;

				delete_user_meta($userid, 'upgrade_request_status');

				delete_user_meta($userid, 'order_id');
			}
		}

		public function before_calculate_price($cart_object)
		{

			global $service_finder_options;

			$deposit = $due = 0;

			foreach ($cart_object->cart_contents as $wc_key => $wc_item) {
				if (isset($wc_item['wooextradata'])) {

					$wootype = $wc_item['wooextradata']['wootype'];

					switch ($wootype) {
						case 'customerupgrade':
							$wc_item['data']->set_price($wc_item['wooextradata']['amount']);
							break;
					}
				}
			}
		}


		public function update_billing_info($null, $field_name)
		{
			$checkout_info = [];

			if (empty($checkout_info)) {

				foreach (WC()->cart->get_cart() as $wc_key => $wc_item) {

					if (array_key_exists('wooextradata', $wc_item)) {



						$wootype = $wc_item['wooextradata']['wootype'];

						switch ($wootype) {

							case 'customerupgrade':

								$userInfo = service_finder_getUserInfo($wc_item['wooextradata']['user_id']);

								$checkout_info = array(

									'billing_first_name' => $userInfo['fname'],

									'billing_last_name'  => $userInfo['lname'],

									'billing_email'      => $userInfo['email'],

									'billing_phone'      => $userInfo['phone'],

									'billing_address_1'      => $userInfo['address'],

									'billing_address_2'      => $userInfo['apt'],

									'billing_city'      => $userInfo['city'],

									'billing_state'      => $userInfo['state'],

									'billing_country'      => $userInfo['country'],

									'billing_postcode'      => $userInfo['zipcode'],

								);

								break;
						}
					}
				}
			}
			if (array_key_exists($field_name, $checkout_info)) {

				return $checkout_info[$field_name];
			}

			return null;
		}

		public function huslajobs_add_to_woo_cart()
		{

			global $service_finder_options; //, $wpdb, $service_finder_Tables;

			$product_id = (isset($service_finder_options['woo-product-id'])) ? esc_html($service_finder_options['woo-product-id']) : '';

			$response = null;

			$extradata = array(

				'expire_limit' => (!empty($_REQUEST['featuredays'])) ? sanitize_text_field($_REQUEST['featuredays']) : 0,
				'amount' => (!empty($_REQUEST['amount'])) ? sanitize_text_field($_REQUEST['amount']) : 0,
				'user_id' => (!empty($_REQUEST['user_id'])) ? sanitize_text_field($_REQUEST['user_id']) : 0,
				'wootype' => (!empty($_REQUEST['wootype'])) ? sanitize_text_field($_REQUEST['wootype']) : '',
				'package_name' => esc_html__('Customer Upgrade', 'service-finder'),

			);

			WC()->cart->empty_cart();

			WC()->cart->add_to_cart($product_id, 1, '', array(), array('wooextradata' => $extradata));

			$response = array('success' => true, 'extradata' => $extradata);

			wp_send_json($response);
		}
	}
	new CU_Woopayments();
}
