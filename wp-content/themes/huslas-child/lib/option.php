<?php

/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */
global $service_finder_options;
if (!class_exists('Redux')) {
    return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "service_finder_options";

// This line is only for altering the demo. Can be easily removed.
// $opt_name = apply_filters('redux_demo/opt_name', $opt_name);


$payment_methods = Redux::getField($opt_name, 'payment-methods');
$payment_methods['options']['momo'] = esc_html__('Mobile Money', 'service-finder');
$payment_methods['options']['orange'] = esc_html__('Orange Money', 'service-finder');
$customerreplacestring = (!empty($service_finder_options['customer-replace-string'])) ? $service_finder_options['customer-replace-string'] : esc_html__('Customers', 'service-finder');


Redux::setField($opt_name, $payment_methods);

//Momo Settings
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'momo-settings',
    'type'     => 'section',
    'title'    => esc_html__('MoMo Settings', 'service-finder'),
    'indent'   => false,
));
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'enable-momo',
    'type'     => 'checkbox',
    'title'    => esc_html__('Enable Mobile Money', 'service-finder'),
    'default'  => '1'
));
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'momo-type',
    'type'     => 'radio',
    'title'    => esc_html__('MoMo API Enviroment', 'service-finder'),
    'options'  => array(
        'sandbox' => esc_html__('Use sandbox (virtual) enviroment to testing Mobile Money functionality', 'service-finder'),
        'mtncameroon'       => esc_html__('Use live (real) enviroment ', 'service-finder'),
    ),
    'default'  => 'sandbox'
));
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'momo-apiuser',
    'type'     => 'text',
    'title'    => esc_html__('MoMo API User', 'service-finder'),
));
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'momo-apikey',
    'type'     => 'text',
    'title'    => esc_html__('MoMo API Key', 'service-finder'),
));
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'momo-subscriptionkey',
    'type'     => 'text',
    'title'    => esc_html__('MoMo Subscription Key', 'service-finder'),
));


//Orange Money Settings
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'orange-settings',
    'type'     => 'section',
    'title'    => esc_html__('Orange Settings', 'service-finder'),
    'indent'   => false,
));
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'enable-orange',
    'type'     => 'checkbox',
    'title'    => esc_html__('Enable Orange Money', 'service-finder'),
    'default'  => '1'
));

Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'orange-appid',
    'type'     => 'text',
    'title'    => esc_html__('App Id', 'service-finder'),
));
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'orange-clientid',
    'type'     => 'text',
    'title'    => esc_html__('Client Id', 'service-finder'),
));
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'orange-clientsecret',
    'type'     => 'text',
    'title'    => esc_html__('Client Secret', 'service-finder'),
));
Redux::setField($opt_name, array(
    'section_id' => 'payment-settings',
    'id'       => 'orange-merchantkey',
    'type'     => 'text',
    'title'    => esc_html__('Merchant Key', 'service-finder'),
));


Redux::setSection($opt_name, array(
    'title'            => esc_html__('Customers Upgrade Settings', 'service-finder'),
    'id'               => 'customers-upgrade-settings-page',
    'customizer'           => false,
    'desc'             => '',
    'customizer_width' => '400px',
    'icon'             => 'el el-cog'
));
Redux::setSection($opt_name, array(
    'title'            => esc_html__('Customers Upgrade', 'service-finder'),
    'id'               => 'customers-upgrade-settings',
    'subsection'       => true,
    'customizer'           => false,
    'customizer_width' => '450px',
    'desc'             => '',
    'fields'           => array(
        array(
            'id'       => 'customer-upgrade',
            'type'     => 'switch',
            'title'    => esc_html__('Upgrade', 'service-finder') . ' ' . esc_html($customerreplacestring),
            'subtitle' => '',
            'default'  => false
        ),
        array(
            'id'            => 'customer-upgrade-price',
            'type'          => 'text',
            'title'         => esc_html__('Price($)/year', 'service-finder'),
            'subtitle'      => esc_html__('Please set the price per year', 'service-finder'),
            'desc'          => '',
            'default'       => '0.01',
        ),
        array(
            'id'            => 'customer-upgrade-price-2',
            'type'          => 'text',
            'title'         => esc_html__('Price($)/year', 'service-finder'),
            'subtitle'      => esc_html__('Please set the price per year', 'service-finder'),
            'desc'          => '',
            'default'       => '0.01',
        ),
        array(
            'id'       => 'minify-css-6',
            'type'     => 'switch',
            'title'    => esc_html__('Minify CSS', 'service-finder'),
            'subtitle' => '',
            'default'  => false,
        ),
    )
));
