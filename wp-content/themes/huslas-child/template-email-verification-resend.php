<?php
/**
 Template name: Email Verify resend template
 */

get_header(); ?>


<?php 
if(isset($_POST['mail_verification_resend']) && !empty($_POST['resend_mail'])){
	$user_mail_u = $_POST['resend_mail'];
	
	if(empty($_POST['resend_mail'])){
        echo '<div class="eml_error">Email cannot be empty.</div>';
    }
    else if(!filter_var($_POST['resend_mail'], FILTER_VALIDATE_EMAIL)){
        echo '<div class="eml_error">Your email address is not valid.</div>';
    }
 
    else{
	$user_mail_object = get_user_by('email', $user_mail_u);
	if($user_mail_object){	
	$user_id = $user_mail_object->ID;
	$users_roles=$user_mail_object->roles;
	if(in_array('pending_customer',$users_roles) || in_array('pending_provider',$users_roles))
	{
		$code = sha1( $user_id . time() );
		$user_info = get_userdata($user_id);
        $activation_link = add_query_arg( array( 'key' => $code, 'user' => $user_id ), get_permalink( 2617 ));
        update_user_meta( $user_id, 'has_to_be_activated', $code);
        wp_mail( $user_mail_u, 'Huslas Email Activation', 'Hello '.$user_info->user_login.',<br> Here is your activation link: ' . $activation_link );
		 echo '<div class="success_resend">Activation link sent successfully.</div>';
	}
	
		
	}else{
		
		echo '<div class="eml_error">User not registered.</div>';
		}
		
	}
	
	
	}


?>

<div class="email_verify_section afclr">
<div class="wrapper">
<div class="email_verify_resend_se_inner afclr">
<form action="" method="post">
<div class="email_resend_form afclr">
<div class="email_resend_f_inp afclr">
<div class="form-group">
<label class="provd_label_heading">Email address</label>
<input type="email" class="form-control" required name="resend_mail"  placeholder="Enter mail address">
</div>

</div>
<div class="email_resend_f_btn afclr"><input type="submit" value="Resend" name="mail_verification_resend"></div>


</div>
</form>

</div>
</div>
</div>


<?php get_footer(); ?>